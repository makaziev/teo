<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "project".
 *
 * @property int $id
 * @property int|null $user1_id user1.id
 * @property string|null $name
 * @property int|null $cost
 * @property int|null $start_date
 * @property int|null $end_date
 *
 * @property User1 $user1
 */
class Project extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user1_id', 'cost'], 'integer'],
//            [['start_date', 'end_date'], 'date'],
            [['name'], 'string', 'max' => 255],
            [['user1_id'], 'exist', 'skipOnError' => true, 'targetClass' => User1::className(), 'targetAttribute' => ['user1_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user1_id' => 'User1 ID',
            'name' => 'Name',
            'cost' => 'Cost',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
        ];
    }

    /**
     * Gets query for [[User1]].
     *
     * @return ActiveQuery
     */
    public function getUser1()
    {
        return $this->hasOne(User1::className(), ['id' => 'user1_id']);
    }
}
