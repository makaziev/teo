<?php

use app\models\User1;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user1_id')->dropDownList(
            ArrayHelper::map(User1::find()->all(), 'id', 'login'),
            [
//                'options' => [
//                    $model->user1 => ['selected' => true]
//                ],
                'prompt' => 'Выберите пользователя',
            ]
    )->label('User') ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cost')->textInput() ?>

    <?= $form->field($model, 'start_date')->widget(DatePicker::className(), [
        'pluginOptions' => [
            'format' => 'dd-mm-yyyy',
        ]
    ]) ?>

    <?= $model->start_date ? $form->field($model, 'end_date')->widget(DatePicker::className(), [
        'pluginOptions' => [
            'format' => 'dd-mm-yyyy',
        ]
    ]) : '' ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
