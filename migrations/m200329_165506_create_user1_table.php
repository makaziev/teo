<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m200329_165506_create_user1_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user1}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(),
            'login' => $this->string(),
            'password' => $this->string(),
        ]);

        $this->insert('{{%user1}}', [
            'username' => 'admin',
            'login' => 'admin',
            'password' => 'admin',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%user1}}', ['id' => 1]);
        $this->dropTable('{{%user1}}');
    }
}
