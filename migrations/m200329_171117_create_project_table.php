<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%project}}`.
 */
class m200329_171117_create_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%project}}', [
            'id' => $this->primaryKey(),
            'user1_id' => $this->integer()->comment('user1.id'),
            'name' => $this->string(),
            'cost' => $this->integer(),
            'start_date' => $this->integer(),
            'end_date' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-project-user1_id',
            '{{%project}}',
            'user1_id',
            '{{%user1}}',
            'id',
            'CASCADE'
        );

        $this->execute('SET foreign_key_checks = 1');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->dropForeignKey('fk-project-user1_id', '{{%project}}');
        $this->dropTable('{{%project}}');
        $this->execute('SET foreign_key_checks = 1');
    }
}
